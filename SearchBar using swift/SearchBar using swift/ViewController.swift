//
//  ViewController.swift
//  SearchBar using swift
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating {

    @IBOutlet weak var showTableView: UITableView!
    var searchController:UISearchController!
    var showRecords:NSArray = []
    var searchFilterArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showRecords = ["One", "Two", "Three", "Four", "Five"];
        
        searchController = UISearchController(searchResultsController: nil)
        searchController!.searchBar.delegate = self
        searchController!.searchResultsUpdater = self
        searchController!.dimsBackgroundDuringPresentation = false
        
        showTableView.tableHeaderView = searchController!.searchBar
        showTableView.addSubview(searchController!.searchBar)
        searchFilterArray = showRecords
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchStr:NSString = searchController.searchBar.text! as String
        
        if searchStr.length > 0 {
            let predicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
            let array = showRecords.filteredArrayUsingPredicate(predicate)
            showRecords = array as Array
            showTableView.reloadData()
        }
        
        else {
            showRecords = searchFilterArray
        }
        
        // Reload the tableview.
        showTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showRecords.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier:NSString = "recordsCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        
        cell.textLabel?.text = showRecords.objectAtIndex(indexPath.row) as? String
        
        return cell
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        showRecords = searchFilterArray
        showTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

